++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
README file for Homework, Math 471, Fall 2020, E.Perez
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

This repository belongs to Elijah Perez.
Each folder corresponds to a homework assignment or lab assignment.

Labs:
You will find lab assignment 1 in the subdirectory lab1.

Homeworks:
You will find Homework 1 in the subdirectory HW1.
You will find Homework 2 in the subdirectory HW2.
You will find Homework 3 in the subdirectory HW3.
You will find Homework 4 in the subdirectory HW4.
You will find Homework 5 in the subdirectory HW5.
You will find Homework 6 in the subdirectory HW6.
You will find Homework 7 in the subdirectory HW7.
